public class Exercicio13 {
   public static void executar(){
      int n = Prompt.lerInteiro("digite quantos numeros quer imprimir da sequencia de fibonacci: ");
      int[] fibonacci = new int [n];

      for(int i = 0; i < n; i++){

        if(i == 0){
          fibonacci[i] = 0;
        }
        else if (i == 1){
          fibonacci[1] = 1;
        }
        else{
          fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }  
      }
     
      for(int i = 0; i < n; i++){
         Prompt.imprimir("a sequencia e : " + fibonacci[i] + " ");
      }
       
   }
}
