public class Exercicio12 {
    public static void executar(){
        int[] num1 = new int[12];
        int somaMult = 1;

        for(int i = 0; i < 12; i++){
            num1[i] = Prompt.lerInteiro("digite seus numeros: ");

            if( num1[i] > 0 && num1[i] % 2 == 0){
                somaMult = somaMult * num1[i];
            }
        }

        Prompt.imprimir("a multiplicacao dos numeros validos e: " + somaMult);
    }

}
