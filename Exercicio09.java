public class Exercicio09 {
    public static void executar(){
        int[] num1 = new int[5];
        int[] num2 = new int[5];
        int[] num3 = new int[10];

        for(int i = 0; i < 5; i++){
            num1[i] = Prompt.lerInteiro("digite o numero da posicao " + (i + 1) + " do vetor1: ");
            num2[i] = Prompt.lerInteiro("digite o numero da posicao " + (i + 1) + " do vetor2: ");
        }

        for(int i = 0; i < 5; i++){

          num3[2 * i] = num1[i];
          num3[2 * i + 1] = num2[i]; 
        }
        
        for(int i = 0; i < 10; i++){
         
            Prompt.imprimir("o vetor " + i + " tem o numero: " + num3[i]);
        }
        
    }
}