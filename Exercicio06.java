public class Exercicio06 {
    public static void executar(){

        Double[] notas = new Double[5];
        Double[] pesos = new Double[5];
        Double[] mult = new Double[5];
        Double somaMult = 0.0;
        Double somaPeso = 0.0;

        for(int i = 0; i < 5; i++){
            notas[i] = Prompt.lerDecimal("digite sua " + (i+1) + " nota: ");
            pesos[i] = Prompt.lerDecimal("digite o peso " + (i+1) + " da media: ");
            mult[i] = notas[i] * pesos[i];
            somaMult = somaMult + mult[i];
            somaPeso = somaPeso + pesos[i];
        }

        Double media = somaMult / somaPeso;
        Prompt.imprimir("sua media e: " + media);

    }

}
