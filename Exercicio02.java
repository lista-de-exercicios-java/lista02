public class Exercicio02 {
    public static void executar(){

         int vezes = Prompt.lerInteiro("digite o numero de vezes que quer digitar o numero: ");

         int[] num = new int[vezes];

         for(int i = 0; i < vezes ; i++){
            num[i] = Prompt.lerInteiro("digite seu numero: ");
            Prompt.imprimir("seu numero esta na posiçao: " + i);

            if(num[i] > 0){
                Prompt.imprimir("seu numero e positivo.\n");
            }
            else if (num[i] == 0){
                Prompt.imprimir("seu numero e 0.\n");
            }
            else if(num[i] < 0){
                Prompt.imprimir("seu numero e negativo.\n");
            }
         }

    }
}
