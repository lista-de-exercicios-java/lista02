public class Exercicio07 {
    public static void executar(){

        int[] num1 = new int[5];
        int[] num2 = new int[5];
        int a = 0;

        for(int i = 0; i < 5; i++){

            num1[i] = Prompt.lerInteiro("digite seu numero do vetor1: ");
            num2[i] = Prompt.lerInteiro("digite seu numero do vetor2: ");

            if(num1[i] == num2[i]){
                 a = a + 1;
            }
        }

        if(a == 5){
                Prompt.imprimir("seus vetores tem o mesmo valor");
        }
        else{
                Prompt.imprimir("seus vetores tem valores diferentes");
        }
    }
}
