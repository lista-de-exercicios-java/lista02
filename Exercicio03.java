public class Exercicio03 {
    public static void executar(){

         int vezes = Prompt.lerInteiro("digite o numero de vezes que quer digitar o numero: ");

        int[] num1 = new int[vezes];
        int[] num2 = new int[vezes];

        for(int i = 0; i < vezes; i++){
            num1[i] = Prompt.lerInteiro("digite seu numero: ");
            num2[i] = num1[i] * 2;

            Prompt.imprimir("o dobro do seu numero e: " + num2[i]);
        }

    }

}
