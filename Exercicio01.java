public class Exercicio01 {
    public static void executar(){

        Double[] num1 = new Double [5];
        Double somaMedia = 0.0;
        Double media = 0.0;

        for(int i = 0; i < 5; i++){
            num1[i] = Prompt.lerDecimal("digite seu " + (i+1) + " numero: ");
            somaMedia += num1[i];
        }

        media = somaMedia / 5;
        Prompt.imprimir("sua media e: " + media + "\n");

        for(int i = 0; i < 5; i++){
         if(num1[i] < media){
            Prompt.imprimir("seu" + (i+1) + "numero esta abaixo da media.");
         }
         else if (num1[i] == media) {
            Prompt.imprimir("seu" + (i+1) + "numero e igual a media");
         }
         else if( num1[i] > media){
            Prompt.imprimir("seu " + (i+1) + "numero e maior que a media.");
         }

        }

    }

}
