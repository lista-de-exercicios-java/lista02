public class Exercicio14 {
    public static void executar(){

        int[] num1 = new int[5];
        boolean troca;

        for(int i = 0; i < 5; i++){
            num1[i] = Prompt.lerInteiro("digite seu numero: ");
        }

        do{

            troca = false;

            for(int i = 0; i < 5 - 1; i++) {

            if (num1[i] > num1[i + 1]){

            int aux = num1[i];
            num1[i] = num1[i +1];
            num1[i + 1] = aux;
            troca = true;
            }

        }

    }while (troca == true);

    Prompt.imprimir("\n ");

    for(int i = 0; i < 5; i++) {

       Prompt.imprimir(num1[i] + " ");

    }

  }

}
